require "./utils.rb"
class Author
    def initialize
        @utils = Utils.new("lib_data.yml")
    end

    def show_authors
        data = @utils.load_file
        data[:authors].each do |hash|
           puts (hash[:name]).to_s + " " + (hash[:biography]).to_s
        end   
    end

    def add_author
        puts "Please enter Author's name"
        data = @utils.load_file
        author = gets.chomp
        puts "Please enter Author's biography"
        bio = gets.chomp
        data[:authors] << {"name": author, "biography": bio}
        @utils.write_file(data)
    end

    def del_author
        puts "Please enter Author's number to delete: "
        data = @utils.load_file
        del_auth = gets.to_i
        data[:authors].delete_at(del_auth - 1)
        @utils.write_file(data)
    end

end