require "./utils.rb"
class Reader
    def initialize
        @utils = Utils.new("lib_data.yml")
    end

    def show_readers
        data = @utils.load_file
        data[:readers].each do |hash|
           puts (hash[:name]).to_s + " " + (hash[:email]).to_s + " " + (hash[:city]).to_s + " " + (hash[:street]).to_s + " " + (hash[:house]).to_s
        end   
    end

    def add_reader
        puts "Please enter Reader's name"
        data = @utils.load_file
        r_name = gets.chomp
        puts "Please enter Reader's email"
        email = gets.chomp
        puts "Please enter Reader's city"
        city = gets.chomp
        puts "Please enter Reader's street"
        street = gets.chomp
        puts "Please enter Reader's house"
        house = gets.chomp

    
        data[:readers] << {"name": r_name, "email": email, "city": city, "street": street, "house": house}
        @utils.write_file(data)
    end

    def del_reader
        puts "Please enter Reader's number to delete: "
        data = @utils.load_file
        del_reader = gets.to_i
        data[:readers].delete_at(del_reader - 1)
        @utils.write_file(data)
    end

end