class Todo
    def initialize
        @utils = Utils.new
    end
    def show_tasks
        data = @utils.load_file
        data.each do |hash|
           puts (data.index(hash).to_i + 1).to_s + " " + hash[:date] + " " + hash[:title]
        end 
    end
    def add_task
        puts "Please enter task name"
        data = @utils.load_file
        task = gets.chomp
        time = Time.new
        data.push({"date": time.strftime("%Y-%m-%d %H:%M:%S"), "title": task})
        @utils.write_file(data)
    end
    def delete_task
        puts "Please enter task number to delete"
        del_task = gets.to_i
        data = @utils.load_file
        data.delete_at(del_task - 1)
        @utils.write_file(data)
    end
end