require "yaml"
class Utils
    @@path = "tasks.yml"
    def load_file
        a = YAML.load_file @@path
    end
    def write_file(data)
        file = File.open(@@path, 'w')
        file.write(data.to_yaml)    
        file.close
    end
end